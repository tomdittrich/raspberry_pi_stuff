Im folgenden Text geht es darum, wie ein separater Rechner (Raspberry Pi) als VPN Gateway für das gesamte Netzwerk fungiert. Der normale Internettraffic eines Clients wird an den Pi geschickt. Dieser verpackt die Informationen in den VPN Mantel und schickt diese über den Router > WAN ins Netz. <br>
Der Vorteil liegt hier bei der einfachen Umsetzung. Für den Pi wird kein zusätzliches Hardware Interface benötigt. <br>
Letztendlich wird im Router (hier Tomato) der Pi als Standard Gateway bei DHCP gesetzt. Jeder DHCP Client bekommt so den richtigen Gateway.

## Quelle 
https://raspberrypi.stackexchange.com/questions/7487/pi-as-a-vpn-router-for-local-machines

## meine Umgebung

**Pi**

+ Model: Pi 3 Model B
+ OS: Ubuntu 16.04 Headless
+ IP: 192.168.1.13

**Router**

+ Asus RT-N18U
+ Tomato 1.28.0000 -3.5-140 K26ARM USB AIO-64K
+ WAN: Cable Modem
+ IP: 192.168.1.1

**Clients**

+ verschiedene Systeme (Linux, Win, Android, iOS)
+ DHCP über Router

## Pi/Droid Gateway ändern
Ist notwendig, damit Pi sich später nicht per DHCP sich selbst als Gateway holt.

### Methode 1: statische IP
+ find your network interface with `ifconfig`
+ edit the config `sudo nano /etc/network/interfaces` :
```bash
# replace "eth0" with your network interface 
# "gateway" is the standard router gateway
iface eth0 inet static
address 192.168.1.13
netmask 255.255.255.0
network 192.168.1.0
broadcast 192.168.1.255
gateway 192.168.1.1
```
Ich hatte Probleme damit, meinem Pi eine statische Adresse zu geben. Im Router lief bereits eine MAC/IP Zuweisung. Methode1 habe ich nicht genutzt. Das führt uns zu Methode2:

### Methode 2: Standard Gateway ändern
Der Pi holt sich seine Adresse weiterhin per DHCP. Dort wird (später) das Gateway auf den Pi gesetzt. Die Folge: ein feedback. Daher wird im Pi das Gateway manuell ueberschrieben.

+ temporäre Änderung `sudo route add default gw 192.168.1.1`
+ mit `route` gegenpruefen, ob die Adresse uebernommen wurde
+ dauerhafte Änderung `sudo echo "supersede routers 192.168.1.1;" >> /etc/dhcp/dhclient.conf`

## OpenVPN einrichten
+ `sudo apt install openvpn`
+ kopieren von .config Datei in */etc/openvpn/*
+ ggf. "auth-pass" anpassen
+ `redirect-gateway` hinzufügen: `sudo echo "redirect-gateway" >> /etc/openvpn/ivpn.conf`
+ Config testen: `cd /etc/openvpn && openvpn ivpn.conf`
    + wird SSH Fenster geschlossen, ist die Verbindung wieder weg
+ Verbindung sollte nach Neustart hergestellt werden > ausprobieren
    + `curl ifconfig.co`
+ erst weitermachen, wenn der Pi sich verbinden kann

### Troubleshooting
Aus irgend einem Grund hat die */etc/openvpn/update-resolv.conf* bei mir nicht geklappt. Die Folge war, dass mein Pi keinen anständigen DNS bzw. keine Host Namen aufloesen konnte. Ich habe die Datei geloescht/umbenannt, dann ging es. 

## NAT am Pi einstellen
+ `sudo iptables -t nat -A POSTROUTING -o tun0 -j MASQUERADE`
+ dann für dauerhafte Einstellung:
+ `iptables-save > /etc/iptables.up.rules`
+ `sudo nano /etc/network/if-pre-up.d/iptables` befüllen:
```bash
#!/bin/bash
/sbin/iptables-restore < /etc/iptables.up.rules
```
+ ausführbar machen: `sudo chmod +x /etc/network/if-pre-up.d/iptables`
+ `sudo nano /etc/sysctl.conf` die Zeile `net.ipv4.ip_forward = 1` ENTkommentieren
+ Pi neustarten, läuft openvpn noch?
+ Pi ist jetzt bereit, Anfragen zu verarbeiten

## Clients einrichten
Steht nicht die Möglichkeit zur Verfügung, den GW per Router & DHCP zu verteilen, muss dieser bei jedem Client manuell eingerichtet werden.

+ temporäre Änderung `sudo route add default gw 192.168.1.13`
+ mit `route` gegenpruefen, ob die Adresse uebernommen wurde
+ dauerhafte Änderung `sudo echo "supersede routers 192.168.1.13;" >> /etc/dhcp/dhclient.conf`

## Router einrichten
Folgend die Einstellungen für einen Tomato Server, um per DHCP den Standard Gateway zu verteilen.

+ per Terminal: `nano /etc/dnsmasq.conf` um `dhcp-option=tag:br0,3,192.168.1.13` erweitern
    + ggf. muss das "tag:br0" raus
    + nach Neustart oder Änderungen auf der GUI wird der Eintrag bei mir zurückgesetzt
+ per GUI: Advanced Settings > DHCP/DNS > DNSmasq custom configuration > `dhcp-option=tag:br0,3,192.168.1.13`
    + doppelt den Eintrag *dhcp-option*, geht aber bei mir da Eintrag an letzter Stelle

## Nachteile
Bricht die VPN Verbindung auf dem Pi ab, springt dieser auf die normale Verbindung zurück. Ein "FallBack" Mechanismus muss her.

## ToDo
+ Mechanismus bei VPN Abbruch
+ DNS laeuft noch über Router mit custom DNS Einträgen
    + DNS ueber Pi / VPN